package data;

import dtos._;
import play.api.libs.json._;
import play.api.libs.ws._;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;
import scala.util.chaining._;

case class DatabaseConfiguration(baseUrl: String, wsClient: WSClient);

object Database
{

  implicit val ec: ExecutionContext = ExecutionContext.global;
  val register: (DatabaseConfiguration, SignUp) => Future[WSResponse] =
    (config, dto) =>
  {
    //no trailing slash! important!
    config.wsClient.url(config.baseUrl.concat("/rpc/register"))
      .addHttpHeaders("Content-Type" -> "application/json")
      .post(Json.obj("username" -> dto.username, "password" -> dto.password));
  };

  val signIn: (DatabaseConfiguration, SignIn) => Future[WSResponse] =
    (config, dto) =>
  {
    //no trailing slash! important!
    config.wsClient.url(config.baseUrl.concat("/rpc/login"))
      .addHttpHeaders("Content-Type" -> "application/json")
      .post(Json.obj("username" -> dto.username, "password" -> dto.password));
  };
}
