package controllers.api;

import javax.inject._;
import play.api._;
import play.api.libs.json._;
import play.api.libs.ws._;
import play.api.mvc._;
import scala.concurrent.ExecutionContext;
import scala.concurrent._;
import scala.util.chaining._;
import web._;
import web.cookie.HgSessionCookie;
import web.models._;

@Singleton
class StateController @Inject()(
    configuration: Configuration,
    val controllerComponents: ControllerComponents
  ) extends BaseController
{
  def get = Action
  {
    request: Request[_] =>
    {
      configuration.pipe(config => Conf.getInstance(config))
        .pipe(conf => HgSessionCookie.fromCookie(conf, request.session))
        .pipe(cs => ClientSession.toJson(cs))
        .pipe(jsValue => Ok(jsValue));
    };
  };
}

