package controllers.api;

import data._;
import data.dtos.SignUp;
import javax.inject._;
import logic.Registration;
import play.api._;
import play.api.libs.json._;
import play.api.libs.ws._;
import play.api.mvc._;
import scala.concurrent.ExecutionContext;
import scala.concurrent._;
import scala.util.chaining._;
import web._;
import web.resultMonad;
import web.resultMonad.ResultMonad;
import web.models._;

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class SignUpController @Inject()
  (configuration: Configuration,
    val controllerComponents: ControllerComponents,
    ws: WSClient) extends BaseController
{
  implicit val ec: ExecutionContext = ExecutionContext.global;

  def act = Action.async(parse.tolerantJson)
  {
    request: Request[JsValue] =>
    {
      web.parsing.Registration.toModel(request.body)
        .map(model => logic.Registration.toDTO(model))
        .flatMap(validated => ResultMonad.fromValidated(validated))
        .map(dto => handleRegistration(dto))
        .pipe(ResultMonad.invertFuture)
        .map(rm => rm.flatMap(rm => rm))
        .map(monad => monad.map(sess =>
          clientSession(configuration, sess.username)
        ))
        .map(m => m.map(clientSession => ClientSession.toJson(clientSession)))
        .map(monad => monad.toResult(json => Created(json)));
    }
  };

  /*--------------------------helpers-------------------------*/
  private def clientSession(configuration: Configuration, username: String):
      models.ClientSession =
  {
    models.Conf.getInstance(configuration)
      .pipe(conf => ClientSession.fromConf(conf))
      .pipe(cs => ClientSession.signIn(cs, username));
  };

  private def dbConfig =
    DatabaseConfiguration(configuration.get[String]("postgrestUrl"), ws);

  private def handleRegistration(dto: dtos.SignUp):
      Future[ResultMonad[models.Session]] =
  {
    Database.register(dbConfig, dto) pipe
    (future => future.map(resp => ResultMonad.fromWSResponse(resp))) map
    (monad => handleRestResponse(monad)) map
    (monad => monad.map(token => models.Session(dto.username, token)))
  };

  private def handleRestResponse(monad: ResultMonad[JsValue]):
      ResultMonad[String] =
  {
    monad map
    (jsValue => jsValue.validate[String](web.parsing.Parsers.token)) flatMap
    (jsResult => ResultMonad.fromJsResult(jsResult));
  };
}
