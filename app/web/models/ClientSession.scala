package web.models;

import play.api.libs.json._;
import play.api.mvc;
import scala.util.chaining._;
import web.models._;
import web.parsing.PkceOAuthRequest;

/*"token" is missing here on purpose. ClientSession is meant to be readable by
 * the client--the client doesn't need to know their own database api key.*/

private object Properties
{
  val gitLabPkceOAuthRequest = "gitLabPkceOAuthRequest";
  val username = "username"
}

final case class ClientSession
(
  gitLabClientId: String,
  gitLabPkceOAuthRequest: Option[PkceOAuthRequest],
  gitLabRedirectUrl: String, 
  gitLabOAuthResponse: Option[GitLabOAuthResponse],
  oAuthState: Option[String],
  username: Option[String]
);

private class ClientSessionWrites extends Writes[ClientSession]
{
  val gitLabWrites: Writes[GitLabOAuthResponse] =
    Json.writes[GitLabOAuthResponse];
  val pkceWrites: Writes[PkceOAuthRequest] =
    Json.writes[PkceOAuthRequest];
  def writes(cs: ClientSession): JsValue = Json.obj(
    Properties.username -> (cs.username match {
      case None => JsNull;
      case Some(username) => JsString(username);
    }),
    "gitLab" -> (cs.gitLabOAuthResponse match {
      case None => JsNull;
      case Some(resp) => Json.toJson(resp)(gitLabWrites);
    }),
    "gitLabClientId" -> JsString(cs.gitLabClientId),
    Properties.gitLabPkceOAuthRequest -> (cs.gitLabPkceOAuthRequest match {
      case None => JsNull;
      case Some(pkce) => Json.toJson(pkce)(pkceWrites);
    }),
    "gitLabRedirectUrl" -> JsString(cs.gitLabRedirectUrl),
    "oAuthState" -> (cs.oAuthState match {
      case None => JsNull;
      case Some(oAuthState) => JsString(oAuthState);
    })
  );
}

/*----------------------companion object -----------------------------------*/
object ClientSession
{
  def fromConf(conf: Conf): ClientSession =
    ClientSession(
      username = None,
      gitLabOAuthResponse = None,
      gitLabClientId = conf.gitLabClientId,
      gitLabPkceOAuthRequest = None,
      gitLabRedirectUrl = conf.gitLabRedirectUrl,
      oAuthState = None
    );
  /*
  def fromCookie(conf: Conf, cookie: mvc.Session): ClientSession =
  {
    fromConf(conf)
      .pipe(cs => cs.copy(username=cookie.get(Properties.username)))
      .pipe(cs =>
        cs.copy(gitLabOAuthResponse=GitLabOAuthResponse.fromSession(cookie))
      )
      .pipe(cs => cs.copy(
        gitLabPkceOAuthRequest=getGitLabPkceOAuthRequest(cookie)
      ))
      .pipe(cs => cs.copy(oAuthState=cookie.get("oAuthState")));
  };
   */

  def signIn(prev: ClientSession, username: String): ClientSession =
    prev.copy(username = Some(username));
  
  def toJson(cs: ClientSession) = Json.toJson(cs)(writes);

  def writes: Writes[ClientSession] = new ClientSessionWrites();

  /*-------------------------------------------------------------------------*/

  private def getGitLabPkceOAuthRequest(cookie: mvc.Session):
      Option[PkceOAuthRequest] =
  {
    cookie.get(Properties.gitLabPkceOAuthRequest)
      .map(str => Json.parse(str))
      .map(jsValue =>jsValue.validate(Json.reads[PkceOAuthRequest]))
      .flatMap(jsResult => jsResult.asOpt)
  }
}
