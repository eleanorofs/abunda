package web.models;

import play.api.mvc.Session

final case class PkceOAuthRequest(
  codeChallenge: String,
  codeVerifier: String,
  host: String,
  state: String
);
