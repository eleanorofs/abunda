/* for case classes */

package web.models;

final case class Session(username: String, token: String);

final case class SignInResult(username: String);

final case class SignIn
(
  username: String,
  password: String,
  agreeToTerms: Boolean
);

final case class Registration
(
  username: String,
  password: String,
  confirmPassword: String,
  agreeToTerms: Boolean
);

final case class State(state: String);
