package web.cookie;

import play.api.libs.json._;
import scala.util.chaining._;
import web.models.ClientSession;
import web.models.Conf;
import web.models.GitLabOAuthResponse;
import web.models.PkceOAuthRequest;

object HgSessionCookie
{
  def fromCookie(conf: Conf, cookie: play.api.mvc.Session): ClientSession =
  {
    ClientSession.fromConf(conf)
      .pipe(cs => cs.copy(username=cookie.get(Properties.username)))
      .pipe(cs =>
        cs.copy(gitLabOAuthResponse=GitLabOAuthResponse.fromSession(cookie))
      )
      .pipe(cs => cs.copy(
        gitLabPkceOAuthRequest=getGitLabPkceOAuthRequest(cookie)
      ))
      .pipe(cs => cs.copy(oAuthState=cookie.get(Properties.oAuthState)));
  };

  def setGitLabPkceOAuthRequest(session: play.api.mvc.Session,
    pkce: PkceOAuthRequest): play.api.mvc.Session =
  {
    web.parsing.PkceOAuthRequest.toJson(pkce)
    .pipe(str => session + (Properties.gitLabPkceOAuthRequest -> str))
  };

  def signIn(token: String, username: String): play.api.mvc.Session =
  {
    new play.api.mvc.Session(Map(
      Properties.token -> token,
      Properties.username -> username
    ));
  };

  /*-------------------------------------------------------------------------*/
  private def getGitLabPkceOAuthRequest(cookie: play.api.mvc.Session):
      Option[PkceOAuthRequest] =
  {
    cookie.get(Properties.gitLabPkceOAuthRequest)
      .map(str => Json.parse(str))
      .map(jsValue =>jsValue.validate(Json.reads[PkceOAuthRequest]))
      .flatMap(jsResult => jsResult.asOpt); 
  }
}

private object Properties
{
  val gitLab = "gitLab";
  val gitLabClientId = "gitLabClientId"
  val gitLabPkceOAuthRequest = "gitLabPkceOAuthRequest";
  val gitLabRedirectUrl = "gitLabRedirectUrl";
  val oAuthState = "oAuthState";
  val token = "token";
  val username = "username";
}

private class ClientSessionWrites extends Writes[ClientSession]
{
  val gitLabWrites: Writes[GitLabOAuthResponse] =
    Json.writes[GitLabOAuthResponse];
  val pkceWrites: Writes[PkceOAuthRequest] =
    Json.writes[PkceOAuthRequest];

  def writes(cs: ClientSession): JsValue = Json.obj(
    Properties.username -> (cs.username match {
      case None => JsNull;
      case Some(username) => JsString(username);
    }),
    Properties.gitLab -> (cs.gitLabOAuthResponse match {
      case None => JsNull;
      case Some(resp) => Json.toJson(resp)(gitLabWrites);
    }),
    Properties.gitLabClientId -> JsString(cs.gitLabClientId),
    Properties.gitLabPkceOAuthRequest -> (cs.gitLabPkceOAuthRequest match {
      case None => JsNull;
      case Some(pkce) => Json.toJson(pkce)(pkceWrites);
    }),
    Properties.gitLabRedirectUrl -> JsString(cs.gitLabRedirectUrl),
    Properties.oAuthState -> (cs.oAuthState match {
      case None => JsNull;
      case Some(oAuthState) => JsString(oAuthState);
    })
  );
}

